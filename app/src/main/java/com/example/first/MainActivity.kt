package com.example.first

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    private var text:String = "В магазине <осталось> 10 яблоков а может и <больше кто знает>"

    private var textV1:TextView? = null
    private var textV2:TextView? = null
    private var arr:Array<Int> = arrayOf(1,2,3,4,5,6,7)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        textV1 = findViewById(R.id.textView)
        textV2 = findViewById(R.id.textView2)
        for (num in arr){
            print("$num \t")
        }
        val text1:String = text.substringAfter('<').substringBefore('>')
        val text2:String = text.substringAfter('>').substringAfter('<').substringBefore('>')

        textV1?.text = text1
        textV2?.text = text2
    }
}

